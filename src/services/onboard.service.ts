import { FlightsModel } from '../models/flights.model'
import { PersonsModel } from '../models/persons.model'

export class OnboardService {
    async onboardPerson(personId:string,fligthId:string) {
        const isExistingPerson = await this.validatePerson(personId)
        if (!isExistingPerson) {
            throw ('this person does not esxist')
        
        }
        const flight = await this.getFlight(fligthId)
        if (!flight) {
            throw ('this flight does not esxist')
        }
        
        if(flight.passengers.includes(personId)){

            throw ('this Person was already added to this  flight')
        }
        flight.passengers.push(personId);
        
        await  FlightsModel.updateOne({_id:fligthId},flight)

        return 'updated'
    }
    async validatePerson(id:string){
        let person = await PersonsModel.findById(id)
        
        return person? true: false

    }
    async getFlight(id:string){
        let flight = await FlightsModel.findById(id)
        return flight

    }
    


}
