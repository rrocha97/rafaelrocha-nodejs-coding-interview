import { JsonController, Post,Body } from 'routing-controllers'
import { OnboardService } from '../services/onboard.service'

const onboardService = new OnboardService()

@JsonController('/onboard')
export default class OnboardController {
    @Post('/', { transformResponse: false })
    async onboardPerson(@Body() body:any) {
        return {
            status: 200,
            data: await onboardService.onboardPerson(body.person,body.fligth),
        }
    }
}
