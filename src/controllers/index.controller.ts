import { JsonController, Get,Req, Res } from "routing-controllers";


@JsonController('/')
export default class IndexController {
    @Get('',{transformResponse: false})
    get(@Req() request: any, @Res() response: any) {
        return response.status(400).send('Hello response!');
    }
}